# Lobsters App

An iOS and Android app for lobste.rs and its sister sites. Contributions welcome.

## Screenshots

![](/screenshots/1.png)
![](/screenshots/2.png)
![](/screenshots/3.png)

## Getting Started

For help getting started with Flutter, view the online
[documentation](https://flutter.io/).

## Building the App

To run, just do a `flutter run`.

To build for release, put your keystore information inside the Android keystore.properties and then `flutter build apk`.